﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
public class Score : MonoBehaviour
{
    public static int scoreValue = 0;
    public static int scoreHight = 0;
    Text score;
    // Start is called before the first frame update
    private void Awake()
    {
        try
        {
            string filePath = Path.Combine(Application.dataPath, "highScore.json");
            if (File.Exists(filePath))
            {
                string dataAsJson = File.ReadAllText(filePath);
                var highScore = JsonUtility.FromJson<HighScore>(dataAsJson);
                scoreHight = highScore.score;
            }
        }
        catch (Exception e)
        {

            Debug.LogError(e.Message);
        }
    }
    void Start()
    {
        score = GetComponent<Text>();


    }

    // Update is called once per frame
    void Update()
    {
        score.text = "Score: " + scoreValue;
        if (scoreValue > scoreHight)
        {
            scoreHight = scoreValue;
            try
            {
                var highScore = new HighScore { score = scoreHight };
                string dataAsJson = JsonUtility.ToJson(highScore);

                string filePath = Path.Combine(Application.dataPath, "highScore.json");
                File.WriteAllText(filePath, dataAsJson);

            }
            catch (Exception e)
            {

                Debug.LogError(e.Message);
            }
        }
    }
}
