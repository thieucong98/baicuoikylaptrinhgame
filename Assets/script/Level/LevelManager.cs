﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelManager : MonoBehaviour
{
    public static int level = 1;
    // Start is called before the first frame update
    
    void Start()
    {
        level = Score.scoreValue / 20 + 1;
    }

    // Update is called once per frame
    void Update()
    {
        if (level == 1)
        {
            enemy.enemySpeed = 1;
        }
        level = Score.scoreValue / 20 + 1;
        GameObject.Find("level").GetComponent<Text>().text = "Level: " + level;
    }
}